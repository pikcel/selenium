from allure_commons.types import Severity
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
import allure


@allure.title('Результаты поиска google')
@allure.severity(Severity.MINOR)
def test_google_search():
    driver = WebDriver(executable_path='D://selenium//chromedriver.exe')

    with allure.step('Открываем страницу google'):
        driver.get('https://google.com.ua')

    with allure.step('Ищем запрос "одежда"'):
        search_input = driver.find_element_by_xpath('//input[@class="gLFyf gsfi"]')
        search_input.send_keys('одежда')
        search_input.send_keys(Keys.ENTER)

    def check_results_count(driver):
        search_results = driver.find_elements_by_xpath('//div[@class="g"]')
        return len(search_results) >= 10
    with allure.step('Ожидаем колличество результатов'):
        WebDriverWait(driver, 5, 0.5).until(check_results_count)

    with allure.step('Переходим по ссылку LaModa'):
        search_results = driver.find_elements_by_xpath('//div[@class="g"]')
        link = search_results[0].find_element_by_xpath('.//div[@class="r"]/a')
        link.click()

    with allure.step('Сравниваем кооректность title'):
        assert driver.title == 'Женская одежда — купить в интернет-магазине Ламода'

